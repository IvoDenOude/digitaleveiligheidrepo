﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

    private readonly int IDLE = 0;
    private readonly int RUNNING = 1;

    bool facingRight;
    float forwardSpeed = 10f;
    private float screenWidth;

    private Vector2 fp; // first finger position
    private Vector2 lp; // last finger position
    private Animator anim;

    // Wanneer het object wordt aangemaakt.
    void Start () {
        screenWidth = Screen.width;
        anim = gameObject.GetComponent<Animator>();
        facingRight = true;
    }

    // Wordt per frame uitgevoerd.
    public float moveSpeed = 5.0F;
    public float jumpSpeed = 100.0F;

    void Update()
    {
        if (Input.touchCount == 1)
        {
            // Pak de touch.
            Touch touch = Input.touches[0];

            // Check welke kant van het scherm wordt aangeraakt.
            if (touch.position.x < (screenWidth / 2))
            {
                transform.position += Vector3.left * moveSpeed * Time.deltaTime;
                FaceLeft();
            }
            else if (touch.position.x > (screenWidth / 2))
            {
                transform.position += Vector3.right * moveSpeed * Time.deltaTime;
                FaceRight();
            }
            anim.SetInteger("State", RUNNING);
        }
        // Twee vingers swipe omhoog.
        else if (Input.touchCount > 1)
        {
            foreach (var touch in Input.touches)
            {
                if (touch.phase == TouchPhase.Began)
                {
                    fp = touch.position;
                    lp = touch.position;
                }
                if (touch.phase == TouchPhase.Moved)
                {
                    lp = touch.position;
                }
                if (touch.phase == TouchPhase.Ended)
                {
                    if ((fp.y - lp.y) < -80) // up swipe
                    {
                        // add your jumping code here
                        //  transform.position += Vector3.up * jumpSpeed * Time.deltaTime;

                        GetComponent<Rigidbody2D>().AddForce(new Vector2(0, 10), ForceMode2D.Impulse);
                    }
                }
            }
        } else
        {
            anim.SetInteger("State", IDLE);
        }
    }

    void FaceRight()
    {
        if (!facingRight)
        {
            facingRight = true;
            Vector3 temp = transform.localScale;
            temp.x *= -1;
            transform.localScale = temp;
        }
    }

    void FaceLeft()
    {
        if (facingRight)
        {
            facingRight = false;
            Vector3 temp = transform.localScale;
            temp.x *= -1;
            transform.localScale = temp;
        }
    }
}

