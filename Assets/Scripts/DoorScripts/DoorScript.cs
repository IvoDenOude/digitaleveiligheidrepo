﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DoorScript : MonoBehaviour {

    public Button nextLevelButton;

	// Use this for initialization
	void Start () {
        nextLevelButton.gameObject.SetActive(false);
        nextLevelButton.GetComponentInChildren<Text>().text = "Volgende kamer!";
        // add isTrigger
        var boxCollider = gameObject.AddComponent<BoxCollider2D>();
        boxCollider.isTrigger = true;
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter2D(Collider2D other) {
        nextLevelButton.gameObject.SetActive(true);
        Debug.Log("entered");
    }

    private void OnTriggerExit2D(Collider2D other) {
        nextLevelButton.gameObject.SetActive(false);
        Debug.Log("exit");
    }
}
