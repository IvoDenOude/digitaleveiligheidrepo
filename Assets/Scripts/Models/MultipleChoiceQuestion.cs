﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts {
    public class MultipleChoiceQuestion {
        public string Question { get; set; }
        public string[] Answers { get; set; }
        public string CorrectAnswer { get; set; }
        public string Feedback { get; set; }

        public MultipleChoiceQuestion() { }

        public MultipleChoiceQuestion(string q, string[] a, string ca, string f) {
            Question = q;
            Answers = a;
            CorrectAnswer = ca;
            Feedback = f;
        }
    }
}
