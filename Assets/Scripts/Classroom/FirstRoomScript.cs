﻿using Assets.Scripts;
using Assets.Scripts.GameScripts;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FirstRoomScript : MonoBehaviour {

    public Button startQuestionsButton;
    public Button exitQuestionsButton;

    public Text questionText;
    public Text amountOfQuestionsText;

    public Button answerButton1;
    public Button answerButton2;
    public Button answerButton3;
    public Button answerButton4;


    public Canvas questionCanvas;
    List<MultipleChoiceQuestion> questions;
    MultipleChoiceQuiz multipleChoiceQuiz;


    // Use this for initialization
    void Start () {
        startQuestionsButton.onClick.AddListener(delegate { OnButtonClick(Buttons.StartButton); });
        exitQuestionsButton.onClick.AddListener(delegate { OnButtonClick(Buttons.ExitButton); });

        answerButton1.onClick.AddListener(delegate { OnButtonClick(Buttons.AnswerButton1); });
        answerButton2.onClick.AddListener(delegate { OnButtonClick(Buttons.AnswerButton2); });
        answerButton3.onClick.AddListener(delegate { OnButtonClick(Buttons.AnswerButton3); });
        answerButton4.onClick.AddListener(delegate { OnButtonClick(Buttons.AnswerButton4); });

        startQuestionsButton.GetComponentInChildren<Text>().text = "Start quiz!";

        questionCanvas.gameObject.SetActive(false);
        questions = GenerateQuestions();
    }

    private List<MultipleChoiceQuestion> GenerateQuestions() {
        var questionList = new List<MultipleChoiceQuestion>();

        questionList.Add(new MultipleChoiceQuestion
        {
            Question = "Welk wachtwoord is het sterkst?",
            Answers = new string[]
            {
                "1234",
                "kaas",
                "0000",
                "*aE^",
            },
            CorrectAnswer = "*aE^",
            Feedback = "Het goede antwoord is *aE^ omdat de aanvaller door de rare tekens meer pogingen moet doen om je wachtwoord te raden"
        });

        questionList.Add(new MultipleChoiceQuestion
        {
            Question = "Wat is het veiligst?",
            Answers = new string[]
            {
                "Je oplaadkabel in een onbekende computer steken",
                "Je oplaadkabel in het stopcontact steken",
                "Je oplaadkabel in je eigen computer steken",
                "Je oplaadkabel in een tv steken",
            },
            CorrectAnswer = "Je oplaadkabel in het stopcontact steken;Je oplaadkabel in je eigen computer steken",
            Feedback = "De goeden antwoorden zijn [2] en [3] omdat....."
        });

        questionList.Add(new MultipleChoiceQuestion
        {
            Question = "Wat moet je doen als je een mailtje niet vertrouwt?",
            Answers = new string[]
            {
                "Niks",
                "Het mailtje openen en alles downloaden",
                "Het mailtje doorsturen naar je vrienden",
                "Het mailtje verwijderen",
            },
            CorrectAnswer = "Het mailtje verwijderen",
            Feedback = "De goede antwoord is het mailtje verwijderen omdat....."
        });

        questionList.Add(new MultipleChoiceQuestion
        {
            Question = "Wat is ransomware?",
            Answers = new string[]
            {
                "Software die je computer overneemt",
                "Een web-browser",
                "Een game die op scholen wordt gespeeld",
                "Een onderdeel van je auto",
            },
            CorrectAnswer = "Software die je computer overneemt",
            Feedback = "De goede antwoord is software die je computer overneemt omdat....."
        });

        return questionList;
    }

    // Update is called once per frame
    void Update () {

    }

    private void OnButtonClick(Buttons buttonId) {
        switch(buttonId) {
            case Buttons.StartButton:
                Debug.Log("StartQuestions");
                questionCanvas.gameObject.SetActive(true);
                startQuestionsButton.gameObject.SetActive(false);

                multipleChoiceQuiz = new MultipleChoiceQuiz(questions);
                amountOfQuestionsText.GetComponentInChildren<Text>().text = (multipleChoiceQuiz.CurrentQuestion + 1).ToString() + "/" + multipleChoiceQuiz.AmountOfQuestions.ToString();
                LoadNextQuestion();
                break;
            case Buttons.ExitButton:
                Debug.Log("ExitQuestions");
                questionCanvas.gameObject.SetActive(false);
                startQuestionsButton.gameObject.SetActive(true);
                break;
        }

        if(multipleChoiceQuiz.CurrentQuestion < multipleChoiceQuiz.AmountOfQuestions) {
            bool answeredCorrectly = false;
            switch(buttonId) {
                case Buttons.AnswerButton1:
                    answeredCorrectly = multipleChoiceQuiz.AnswerQuestion(answerButton1.GetComponentInChildren<Text>().text);
                    
                    break;
                case Buttons.AnswerButton2:
                    answeredCorrectly = multipleChoiceQuiz.AnswerQuestion(answerButton2.GetComponentInChildren<Text>().text);
                    break;
                case Buttons.AnswerButton3:
                    answeredCorrectly = multipleChoiceQuiz.AnswerQuestion(answerButton3.GetComponentInChildren<Text>().text);
                    break;
                case Buttons.AnswerButton4:
                    answeredCorrectly = multipleChoiceQuiz.AnswerQuestion(answerButton4.GetComponentInChildren<Text>().text);
                    break;
            }
            if (multipleChoiceQuiz.CurrentQuestion < multipleChoiceQuiz.AmountOfQuestions) {
                LoadNextQuestion();
                amountOfQuestionsText.GetComponentInChildren<Text>().text = (multipleChoiceQuiz.CurrentQuestion + 1).ToString() + "/" + multipleChoiceQuiz.AmountOfQuestions.ToString();
            }
        }
        if (multipleChoiceQuiz.CurrentQuestion == multipleChoiceQuiz.AmountOfQuestions) {
            Debug.Log("Het is klaar");
            questionCanvas.gameObject.SetActive(false);
        }
    }

    public void LoadNextQuestion() {
        var nextQuestion = multipleChoiceQuiz.NextQuestion();
        if (nextQuestion != null) {
            Debug.Log(nextQuestion.Question);
            questionText.GetComponentInChildren<Text>().text = nextQuestion.Question;

            for (var i = 0; i < nextQuestion.Answers.Length; i++) {
                Debug.Log(nextQuestion.Answers[i]);
                switch (i) {
                    case 0:
                        answerButton1.GetComponentInChildren<Text>().text = nextQuestion.Answers[i];
                        break;
                    case 1:
                        answerButton2.GetComponentInChildren<Text>().text = nextQuestion.Answers[i];
                        break;
                    case 2:
                        answerButton3.GetComponentInChildren<Text>().text = nextQuestion.Answers[i];
                        break;
                    case 3:
                        answerButton4.GetComponentInChildren<Text>().text = nextQuestion.Answers[i];
                        break;
                }
            }
        }
    }

    enum Buttons {
        StartButton = 1,
        ExitButton = 2,
        AnswerButton1 = 3,
        AnswerButton2 = 4,
        AnswerButton3 = 5,
        AnswerButton4 = 6
    }
}
