﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Assets.Scripts.UI {
    public class CanvasScript : MonoBehaviour{

        public Button nextLevelButton;

        void Start() {
            nextLevelButton.onClick.AddListener(delegate { OnButtonClick(1); });
        }


        private void OnButtonClick(int buttonId) {
            if(buttonId == 1) {
                SceneManager.LoadScene("RoomOne");
            }
        }

        // Update is called once per frame
        void Update() {

        }
    }
}
