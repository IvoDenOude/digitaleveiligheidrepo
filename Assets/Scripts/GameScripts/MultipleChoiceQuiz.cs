﻿using System;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.GameScripts {
    public class MultipleChoiceQuiz {
        List<MultipleChoiceQuestion> Questions;
        public int AmountOfQuestions { get; set; }
        public int CurrentQuestion { get; set; }
        int CorrectAnswers;

        public MultipleChoiceQuiz(List<MultipleChoiceQuestion> questions) {
            this.Questions = questions;

            StartQuiz();
        }

        private void StartQuiz() {
            AmountOfQuestions = Questions.Count;
            CurrentQuestion = 0;
            CorrectAnswers = 0;
        }

        public bool AnswerQuestion(string answer) {
            Debug.Log(CurrentQuestion);
            var currentQuestion = Questions[CurrentQuestion];
            if (CheckIfItsTheCorrectAnswer(answer, currentQuestion.CorrectAnswer)) {
                CurrentQuestion++;
                CorrectAnswers++;
                Debug.Log("Feedback: " + currentQuestion.Feedback);
                return true;
            }

            CurrentQuestion++;
            Debug.Log("Fout feedback: " + currentQuestion.Feedback);
            return false;
        } 

        private bool CheckIfItsTheCorrectAnswer(string currentAnswer, string correctAnswer) {
            if(correctAnswer.Contains(";")) {
                var splittedCorrectAnswers = correctAnswer.Split(';');
                foreach(var splittedCorrectAnswer in splittedCorrectAnswers) {
                    if (splittedCorrectAnswer.Equals(correctAnswer)) {
                        return true;
                    }
                }
            }
            else if (currentAnswer.Equals(correctAnswer)) {
                return true;
            }
            return false;
        }

        public MultipleChoiceQuestion NextQuestion() {
            Debug.Log("NextQuestion: " + CurrentQuestion);
            var question = Questions[CurrentQuestion];
            return question;      
        }
    }
}
