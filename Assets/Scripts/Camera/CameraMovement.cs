﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour {

    public GameObject player;

    //private Vector3 offset;

    // Wanneer het object wordt aangeroepen.
    void Start()
    {
        //offset = transform.position - player.transform.position;
    }

    // Iedere frame wordt deze method aangeroepen.
    void LateUpdate()
    {
        Vector3 velocity = Vector3.zero;
        Vector3 forward = player.transform.forward * 10.0f;
        Vector3 needPos = player.transform.position - forward;
        transform.position = Vector3.SmoothDamp(transform.position, needPos,
                                                ref velocity, 0.05f);
        transform.LookAt(player.transform);
        transform.rotation = player.transform.rotation;
    }
}
